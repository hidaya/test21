@extractCandidaturesData
Feature: Candidature Data Extraction
  As an admin, I want to extract all candidature data

  Scenario: Exporting candidates data from each page
    Given the admin is on the home page
    When the admin clicks on connexion
    And the admin enters mouna.makni@talan.com and Admin09$
    And the admin clicks on login button
    And the admin navigates to the candidature section
    When the admin clicks on the export button
    Then the current page candidate data is downloaded as an Excel file
    When the admin checks if there is a next page
    Then the admin navigates to the next page
    And the admin repeats the export process until the last page is reached
