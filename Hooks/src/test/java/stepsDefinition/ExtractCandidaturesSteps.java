package stepsDefinition;

import org.openqa.selenium.WebDriver;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pages.CandidaturePage;

public class ExtractCandidaturesSteps {

	WebDriver driver = Hooks.driver;
	CandidaturePage candidature;

	@When("the admin clicks on the export button")
	public void the_admin_clicks_on_the_export_button() {
	}
	@Then("the current page candidate data is downloaded as an Excel file")
	public void the_current_page_candidate_data_is_downloaded_as_an_excel_file() {
	}
	@When("the admin checks if there is a next page")
	public void the_admin_checks_if_there_is_a_next_page() {
	}
	@Then("the admin navigates to the next page")
	public void the_admin_navigates_to_the_next_page() {
	}
	@Then("the admin repeats the export process until the last page is reached")
	public void the_admin_repeats_the_export_process_until_the_last_page_is_reached() {
	}
}